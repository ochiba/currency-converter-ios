//
//  ExchangeRatesTests.swift
//  ExchangeRatesTests
//
//  Created by Osamu Chiba on 5/29/21.
//

import XCTest
@testable import ExchangeRates

class ExchangeRatesTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        let aud = RateModel(currency: "AUD", rate: 1.50)
        let result = 20 * aud.rate
        XCTAssertTrue(result == 30.0)
    }
    
    func test2() throws {
        let converter = CurrencyConverter()
        converter.currentRate = RateModel(currency: "AUD", rate: 1.50)
        let result = converter.calculateToString(amount: 20.0)
        XCTAssertFalse(result == "30.0") // result == "30.00", 2 decimals.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
