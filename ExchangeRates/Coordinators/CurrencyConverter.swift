//
//  CurrencyConverter.swift
//  ExchangeRates
//
//  Created by Osamu Chiba on 5/29/21.
//

import UIKit
import Foundation

let exchangeRatesUrlKey = "CurrencyUrl"
let apiAccessKey = "AccessKey"
let timestampKey = "Timestamp"
let sourceKey = "Source"

public protocol ConverterDelegate {
    func sourceCurrencySet(_ source: String)
    func showError(errorMessage: String)
}

class CurrencyConverter: NSObject {
    var currentRate: RateModel?
    var rates: [RateModel]?
    
    public func setup(delegate: ConverterDelegate) {
        let dbManager = DatabaseManager()

        // Don't download if the previous list was downloaded within 1/2 hour.
        if isWithin30min() {
            let allRates = dbManager.getAllRates()
            self.rates = allRates.sorted(by: {$0.currency < $1.currency})
            guard let source = UserDefaults.standard.object(forKey: sourceKey) as? String else {
                delegate.showError(errorMessage: "Your location's currency is not defined.")
                return
            }

            delegate.sourceCurrencySet(source)
        } else {
            download(delegate: delegate, dbManager: dbManager)
        }
    }
    
    public func calculate(amount: Double) -> Double {
        guard let currentRate = self.currentRate else {
            return 0.0
        }
        
        return currentRate.rate * amount
    }
    
    public func calculateToString(amount: Double) -> String {
        let value = calculate(amount: amount)
        
        if value == 0.0 {
            return "N/A"
        }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = Locale.current
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        
        return formatter.string(from: NSNumber(value: value)) ?? "N/A"
    }
    
    private func isWithin30min() -> Bool {
        let now = Int(Date().timeIntervalSince1970)
        guard let lastTimeDownloaded = UserDefaults.standard.object(forKey: timestampKey) as? Int else {
            return false
        }
        let halfHour = 60 * 30
        return (lastTimeDownloaded + halfHour) > now
    }
    
    private func download(delegate: ConverterDelegate, dbManager: DatabaseManager) {
        let configurations = NSDictionary(contentsOfFile: Bundle.main.path(forResource: "Info", ofType: "plist")!)!
        
        guard let url = configurations[exchangeRatesUrlKey] as! String?,
              let accessKey = configurations[apiAccessKey] as! String? else {
            delegate.showError(errorMessage: "Access Key and / or URL isn't found.")
            return
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let requestUrl = "\(url)?access_key=\(accessKey)&amp;format=1"
        let callURL = URL.init(string: requestUrl)
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 60.0 // TimeoutInterval in Second
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            guard error == nil,
                  let data = data else {
                delegate.showError(errorMessage: error?.localizedDescription ?? "No data")
                return
            }
                
            do {
                let response = try JSONDecoder().decode(AllRatesResponse.self, from: data)
                print(response)
                
                guard response.result,
                      let quotes = response.quotes else {
                    delegate.showError(errorMessage: "Not successful")
                    return
                }

                // 1) Store timestamp
                UserDefaults.standard.set(response.timestamp, forKey: timestampKey)
 
                // 2) Convert the quotes dictionary to an ExchangeRate array, with some clean-up.
                var tempo = [RateModel]()
                
                for (key, value) in quotes {
                    var sanitizedKey = key
                    
                    // Remove the first 3 characters, which is the source "USD"
                    sanitizedKey.removeFirst(3)
                    tempo.append(RateModel(currency: sanitizedKey, rate: value))
                }
                
                // Sort by currency.
                self.rates = tempo.sorted(by: {$0.currency < $1.currency})
                
                if let allRates = self.rates {
                    dbManager.addRates(rates: allRates)
                }
                // 3) This is used to show "USD" or whatever next to the first TextField.
                guard let source = response.source else {
                    return
                }
                delegate.sourceCurrencySet(source)
                UserDefaults.standard.set(source, forKey: sourceKey)

            } catch {
                print("Error 😫 = \(error)")
                delegate.showError(errorMessage: error.localizedDescription)
            }
        }
        
        task.resume()
    }
}
