//
//  DatabaseManager.swift
//  ExchangeRates
//
//  Created by Osamu Chiba on 5/30/21.
//

import UIKit
import CoreData

class DatabaseManager: NSObject {
    private var managedContext: NSManagedObjectContext? = nil
    
    public override init() {
        super.init()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
              return
          }
          
        managedContext = appDelegate.persistentContainer.viewContext
    }
    
    public func addRates(rates: [RateModel]) {
        deleteAllRates()
        rates.forEach { oneRate in
            addRate(rate: oneRate)
        }
    }
    
    public func addRate(rate: RateModel) {
        guard let mo = managedContext else {
            return
        }
        let entity = NSEntityDescription.entity(forEntityName: "Rate", in: mo)!
        let oneRate = NSManagedObject(entity: entity, insertInto: mo)
        
        oneRate.setValue(rate.currency, forKeyPath: "currency")
        oneRate.setValue(rate.rate, forKeyPath: "rate")
        
        do {
            try mo.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    public func getAllRates() -> [RateModel] {
        guard let mo = managedContext else {
            return []
        }
        var array = [RateModel]()
        
        let request = NSFetchRequest<NSManagedObject>(entityName: "Rate")
        
        do {
            let result = try mo.fetch(request)
            
            result.forEach { rate in
                if let currency = rate.value(forKey: "currency") as? String,
                   let rate = rate.value(forKey: "rate") as? Double {
                    array.append(RateModel(currency: currency, rate: rate))
                }
            }
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        return array
    }
    
    private func deleteAllRates() {
        guard let managedContext = self.managedContext else {
              return
          }

        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Rate")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try managedContext.execute(deleteRequest)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}
