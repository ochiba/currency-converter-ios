//
//  RateModel.swift
//  ExchangeRates
//
//  Created by Osamu Chiba on 5/29/21.
//

import Foundation

import UIKit

public struct RateModel: Codable, Equatable {
    public let currency: String
    public let rate: Double
    
    enum CodingKeys: String, CodingKey {
        case currency
        case rate
    }

    public init(currency: String, rate: Double) {
        self.currency = currency
        self.rate = rate
    }
        
    public static func == (lhs: RateModel, rhs: RateModel) -> Bool {
        return lhs.currency == rhs.currency
    }
    
    public var combo: String {
        return "\(currency) (\(rate))"
    }
}
