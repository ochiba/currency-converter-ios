//
//  AllRatesResponse.swift
//  ExchangeRates
//
//  Created by Osamu Chiba on 5/29/21.
//

import Foundation

public struct AllRatesResponse: Decodable {
    let result: Bool
    let terms: String?
    let privacy: String?
    let timestamp: Int?
    let source: String?
    let quotes: [String: Double]?
    
    enum CodingKeys: String, CodingKey {
        case result = "success"
        case terms
        case privacy
        case timestamp
        case source
        case quotes
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.terms      = try values.decode(String.self, forKey: .terms)
        self.privacy    = try values.decode(String.self, forKey: .privacy)
        self.source     = try values.decode(String.self, forKey: .source)
        self.result     = try values.decode(Bool.self, forKey: .result)
        self.timestamp  = try values.decode(Int.self, forKey: .timestamp)
        self.quotes     = try values.decode([String: Double].self, forKey: .quotes)
    }
}
