//
//  ViewController.swift
//  ExchangeRates
//
//  Created by Osamu Chiba on 5/29/21.
//

import UIKit

class ViewController: UITableViewController {
    
    @IBOutlet private weak var yourCurrencyLabel: UILabel!
    @IBOutlet private weak var currencyButton: UIButton!
    @IBOutlet private weak var yourCurrencyField: UITextField!
    @IBOutlet private weak var selectedCurrencyField: UITextField!
    @IBOutlet private weak var equationLabel: UILabel!

    private var converter: CurrencyConverter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        converter = CurrencyConverter()
        converter?.setup(delegate: self)
    }

    @objc
    private func calculate() {
        guard let converter = self.converter,
              let text = yourCurrencyField.text,
              !text.isEmpty,
              let amount = Double(text) else {
            // Um, if the source value is empty, then it's probably better to show "" instead of "N/A".
            selectedCurrencyField.text = ""
            return
        }
        
        selectedCurrencyField.text = converter.calculateToString(amount: amount)
    }
}

extension ViewController: UITextFieldDelegate {
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        perform(#selector(self.calculate), with: nil, afterDelay: 0.5)
    }
    
    @IBAction func currencyButtonTapped(_ sender: UIButton) {
        showCurrencyPicker()
    }
    
    private func showCurrencyPicker() {
        guard let converter = self.converter,
              let list = converter.rates else {
            return
        }
        
        let alert = UIAlertController(title: "Exchange Rates", message: "Please select one", preferredStyle: .actionSheet)
        let oldRate = converter.currentRate
        
        for rate in list {
            let action = UIAlertAction(title: rate.combo, style: .default) { [weak self] (action) in
                
                guard rate != oldRate else { return }
                
                self?.converter?.currentRate = rate
                self?.currencyButton.setTitle(rate.currency, for: .normal)
                self?.calculate()
                self?.yourCurrencyField.becomeFirstResponder()
                self?.updateEquation(rate: rate)
            }
            
            if rate == oldRate {
                action.setValue(true, forKey: "checked")
            }
            
            alert.addAction(action)
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        // The popup list's location must be specified in iPad.  This will be ignored in iPhone, and therefore the list is always displayed
        // at the bottom of screen.
        alert.popoverPresentationController?.sourceView = currencyButton
        
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    private func updateEquation(rate: RateModel) {
        guard let source = yourCurrencyLabel.text else {
            return
        }
        
        equationLabel.text = "1 \(source) = \(rate.rate) \(rate.currency)"
    }
}

extension ViewController: ConverterDelegate {
    func sourceCurrencySet(_ source: String) {
        if !source.isEmpty {
            DispatchQueue.main.async { [weak self] in
                self?.yourCurrencyLabel.text = source
                
                // Force the user to choose a currency immediately.
                self?.showCurrencyPicker()
            }
        }
    }
    
    func showError(errorMessage: String) {
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        
        present(alert, animated: true, completion:nil)
    }
}
